{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "9190095b-9408-473e-9eba-4b1186d23742",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "964f1d65-83bb-4570-9c37-6bb7495499f4",
   "metadata": {},
   "outputs": [],
   "source": [
    "def myfunc(a):\n",
    "    return a *2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "6d6bef41-5ae9-4d97-8556-d9f55c708c80",
   "metadata": {},
   "outputs": [],
   "source": [
    "def myfunc_2(c):\n",
    "    return c*3\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a8e3e5e5-3953-4619-b023-aaffd7cca8e4",
   "metadata": {},
   "outputs": [],
   "source": [
    "def addtwo(a):\n",
    "    return a +2ppwd"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
